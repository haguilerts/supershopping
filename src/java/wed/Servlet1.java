package wed;

import com.google.gson.Gson;
import entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import persistencia.DAO;


    @WebServlet(name = "Servlet1", urlPatterns = {"/Servlet1"})
public class Servlet1 extends HttpServlet {
   
  Gson convertirJson = new Gson();
  ArrayList<Usuario> miListado= new ArrayList();  

   
    
 /////////////////////////////////////////////////////////////   
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
       System.out.println("-----Estas en el doPOST del server---- ");  // esto aparece en la parte de java.
                
                // lo tomo los Json y guardo en una variable pedido
            String pedido=req.getReader().readLine(); 
                // destrasformo el pedido(Json) a lenguaje java y guardo las varible del mismo tipo y nombre q coincidan 
                //de la Usuario.java (class). y todo eso lo guardo dentro una variable "guardado" de tipo Usuario.                
            Usuario guardado= convertirJson.fromJson(pedido,Usuario.class);
                System.out.println("pedido(Jason): "+pedido);// imprimo el formato "Json" en la consola de netbeans
                System.out.println("guardado(Java): "+ guardado);// imprimo el formato "Java" en la consola de netbeans
                //System.out.println("nombre: "+guardado.getNombre());// imprimo esa variable nombre
            
            miListado.add(guardado);
            //resp.getWriter().print(convertirJson.toJson(guardado));

            // respondo con el metodo "resp" en la consola WEB q seria similar a un return(retornar).
            resp.getWriter().println("##### Tu usuario se guardo en nuestra doPost #####");// esto aparece en la web de la parte del servlet.
            resp.getWriter().println("**Json: " +  pedido );
            resp.getWriter().println("**Jaba: " +   guardado);
            resp.getWriter().println("**Jaba (dato): " + guardado.getNombre());
    }    
////////////////////////////////////////////////////////////////////////////////////////////////////
   
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     resp.getWriter().println("Estas consultando el metodo doGet ");// esto aparece en la WEB como mensaje 
            System.out.println("-----Estas en el doGET del server---- ");
     // voy a tomar  la variable "guardado" para delvolver en pantalla x WEB.
     resp.getWriter().println("##### muestro el usuario guardado con el metodo doGET #####");
     resp.getWriter().println(miListado);
            
     //resp.getWriter().println(convertirJson.toJson(miListado));
    // resp.getWriter().println();
     
    }    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
}
