
package compra;

public class Compu {
    String marca;
    String modelo;
    String SO;
    String memoria;
    Double precio;

    public Compu(String marca, String modelo, String SO, String memoria, Double precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.SO = SO;
        this.memoria = memoria;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Compu{" + "marca=" + marca + ", modelo=" + modelo + ", SO=" + SO + ", memoria=" + memoria + ", precio=" + precio + '}';
    }
    
    
    
}
