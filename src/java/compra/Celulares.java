
package compra;

public class Celulares {
    String marca;
    String modelo;
    String SO;
    String camara;
    String memoria;
    String color;
    Double precio;
    
    public Celulares() {}
    public Celulares(String marca, String modelo, String SO, String camara, String memoria, String color, Double precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.SO = SO;
        this.camara = camara;
        this.memoria = memoria;
        this.color = color;
        this.precio = precio;    }

    @Override
    public String toString() {
        return "Celulares{" + "marca=" + marca + ", modelo=" + modelo + ", SO=" + SO + ", camara=" + camara + ", memoria=" + memoria + ", color=" + color + ", precio=" + precio + '}';
    }
     
    
    
     
  
}
